// Javascript Objects

/*
    Objects 
        -an objects is a data type that is used to represent real world objects. It is also a collection of related data and / or functionalities.

    Creating objects using object literal:

    Syntax: 

    let objectName = {
        keyA: valueA,
        keyB: valueB 
    }
*/

let student = {
    firstName: "Rupert",
    lastName: "Ramos",
    age: 30,
    StudentId: "2022-009752",
    email: ["rupert.ramos@gmail.com", "RBR1209@gmail.com"],
    address: {
        street: "125 Ilang-Ilang St.",
        city: "Quezon City",
        country: "Philippines"
    }
}

console.log("Result from creating an object: ")
console.log(student)
console.log(typeof student)

// Creating Objects using constructor function
/*
    Create a reusable function to create several objects that have the same data structure. This is useful for creating multiple instanceds/copies of an object.

    Syntax:
        function objectName(valueA,valueB) {
            this.keyA = valueA
            this.keyB = valueB
        }

        let variable = new function objectName(valueA, valueB)
        console.log(variable)
        - "this" is a keyword that is used for invoking; it refers to the global object
        - don't forget to add "new" keyword when you are creating the variables.`

*/
// We use Pascal Casing for the ObjectNme when creating objects using constructor function
function Laptop(name, manufactureDate) {
    this.name = name
    this.manufactureDate = manufactureDate
}

let laptop = new Laptop("Lenovo", 2008);
console.log("Result of creating objects using object constructor:")
console.log(laptop)

let myLaptop = new Laptop("MacBook Air", [2020, 2021])
console.log("Result of creating objects using object constructor: ")
console.log(myLaptop)

let oldLaptop = new Laptop("Portal R2E CCMC", 1980)
console.log("Result of creating objects using object constructor: ")
console.log(oldLaptop)

// Creating empty object as placeholder

let computer = {}
let myComputer = new Object()
console.log(computer)
console.log(myComputer)

myComputer = {
    name: "Asus",
    manufactureDate: 2012
}

console.log(myComputer)


/*MINI ACTIVITY
    - Create an object constructor function to produce 2 objects with 3 key-value pairs
    - Log the 2 new objects in the console and send SS in our GC.

*/

function friend(name, birthDay, zodiac) {
    this.name = name;
    this.birthDay = birthDay;
    this.zodiac = zodiac;
}

let friend1 = new friend("Maan Cabañgon", "October 13, 1986", "Libra")
let friend2 = new friend("Kena Caguicla", "August 22, 1986", "Leo")

console.log(friend1);
console.log(friend2);


// Accessing Object Property

// Using the dot notation
// Syntax: objectName.propertyName

console.log("Result from dot notation" + myLaptop.name)

// Using the bracket notation
// Syntax: objectName["name"]

console.log("Result from bracket notation" + myLaptop["name"])

// Accessing array objects
let array = [laptop, myLaptop];
// let array = [{name: "Lenovo", manufactureDate: 2008}, {name: Macbook Air, manufactureDate: [2019, 2020]}]


// Dot Notation

console.log(array[0].name)

// Square Bracket Notation
console.log(array[0]["name"])


// Initializing / Adding / Deleting / Reassigning Object Properties

let car = {}

console.log(car);

// Adding object properties

car.name = "Honda Civic"
console.log("Result from adding propert using dot notation: ")
console.log(car)

car["manufacture date"] = 2019
console.log(car)

car.name = ["Ferrari", "Toyota"]
console.log(car)

// Deleting objecting properties
delete car["manufacture date"]

// or    car["manufacture date"] = " "

console.log("Result form deleting object properties.")
console.log(car)

// Reassigning object properties
car.name = "Tesla"
console.log("Result from reassigning object property")
console.log(car)

console.log(" ")


/* Object method
    - a method where a function serves as a value in a property. They are also functions and one of the key differences they have is that methods are gfunction related to a specific object property.

*/
// example
let person = {
    name: "John",
    talk: function() {
        console.log("Hello! My name is " + this.name);
    }
}
console.log(person)
console.log("Result from object methods: ")
person.talk()

// example
person.walk = function() {
    console.log(this.name + " walked 25 steps forward.")
}

person.walk()

// example

let friendName = {
    firstName: "John",
    lastName: "Doe",
    address: {
        city: "Austin, Texas",
        country: "US"
    },
    emails: ["johnD@mail.com", "joe12@yahoo.com"],
    introduce: function() {
        console.log("Hello! My name is " + this.firstName + " " + this.lastName)
    }
}

friendName.introduce()


// Real 

// Using Object Literals

let myPokemon = {
    name: "Pikachu",
    level: 3,
    health: 100,
    attack: 50,
    tackle: function() {
        console.log("This pokemon tackled target Pokemon")
        console.log("Target pokemon's health is now reduced to target Pokemon health")
    },
    faint: function() {
        console.log("Pokemon fainted")
    }
}

console.log(myPokemon)


// Using Object Constructor

function Pokemon(name, level) {

    // Properties
    this.name = name
    this.level = level
    this.health = 3 * level
    this.attack = 2 * level

    // Methods

    this.tackle = function(target) {
            console.log(this.name + " tackled " + target.name)
            console.log(target.name + "'s health is now reduced " + (target.health - this.attack))
            target.health = target.health - this.attack;
            if (target.name <= 5) {
                Pokemon.faint()
            }
        },
        this.faint = function(target) {
            console.log(target.name + " fainted")
        }

}

let charizard = new Pokemon("Charizard", 12)
let squirtle = new Pokemon("Squirtle", 6)

console.log(charizard)
console.log(squirtle)


charizard.tackle(squirtle)


//MAIN ACTIVITY:
/*
WDC028v1.5b-23 | JavaScript - Objects
Graded Activity:
Part 1: 
    1. Initialize/add the following trainer object properties:
      Name (String)
      Age (Number)
      Pokemon (Array)
      Friends (Object with Array values for properties)
    2. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
    3. Access the trainer object properties using dot and square bracket notation.
    4. Invoke/call the trainer talk object method.


Part 2:

1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
    - Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
    (target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function

*/
//Code Here:

console.log(" ")
console.log("Activity")

let trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function() {
        console.log(this.pokemon[0] + "! I choose you!")
    }
}

console.log(trainer)

console.log("Result of dot notation:")
console.log(trainer.name)

console.log("Result of square bracket notation:")
console.log(trainer["pokemon"])

console.log("Result of talk method")
trainer.talk()

// 

function Pokemons(name, level) {

    // Properties
    this.name = name
    this.level = level
    this.health = 2 * level
    this.attack = level

    // Methods

    this.tackle = function(target) {
            console.log(this.name + " tackled " + target.name)
            console.log(target.name + "'s health is now reduced to " + (target.health - this.attack))
            let newTargetHealth = target.health - this.attack;
            if (newTargetHealth <= 5) {
                target.faint();
                return target.health = newTargetHealth;
            } else {
                return target.health = newTargetHealth;
            }
        },
    this.faint = function() {
            console.log(this.name + " fainted.")
        }

}


    let pikachu = new Pokemons("Pikachu", 12)
    let geodude = new Pokemons("Geodude", 8)
    let mewtwo = new Pokemons("Mewtwo", 100)

    console.log(pikachu)
    console.log(geodude)
    console.log(mewtwo)

    geodude.tackle(pikachu)
    console.log(pikachu)
    geodude.tackle(pikachu)
    console.log(pikachu)
    geodude.tackle(pikachu)
    console.log(pikachu)


    mewtwo.tackle(geodude)
    console.log(geodude)